#!/usr/bin/env bash

#/usr/bin/conky -c ~/.config/conky/qtile/01/DoomOne.conf || echo "Couldn't start conky." &
#/usr/bin/emacs --daemon &
#/usr/bin/picom &

## Custom made
# allows seeing last notification with dunst/adds to systray
#~/.local/bin/notifications.py &
# sends notification when battery is low
#~/.local/bin/low_bat &

# Screen Lock setup
#xss-lock -- i3lock -n -i ~/wallpaper.jpg

export LC_ALL="en_US.UTF-8"
