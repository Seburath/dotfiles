import os
import re
import subprocess

from libqtile import qtile, bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, KeyChord, Key, Match, Screen

from libqtile.lazy import lazy
from libqtile.utils import guess_terminal
from libqtile.widget.battery import Battery, BatteryState
from libqtile.widget.volume import Volume

from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

mod = "mod1"  # Sets mod key to SUPER
home = os.path.expanduser("~")  # Allow using 'home +' to expand ~
myBrowser = "firefox"
myFilemgr = "thunar"
myEditor = "emacs"
terminal = "terminator"

keys = [
    Key([mod], "h", lazy.layout.left()),
    Key([mod], "l", lazy.layout.right()),
    Key([mod], "j", lazy.layout.down()),
    Key([mod], "k", lazy.layout.up()),
    Key([mod, "shift"], "h", lazy.layout.shuffle_left()),
    Key([mod, "shift"], "l", lazy.layout.shuffle_right()),
    Key([mod, "shift"], "j", lazy.layout.shuffle_down()),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up()),
    Key([mod, "control"], "h", lazy.layout.grow_left()),
    Key([mod, "control"], "l", lazy.layout.grow_right()),
    Key([mod, "control"], "j", lazy.layout.grow_down()),
    Key([mod, "control"], "k", lazy.layout.grow_up()),

    Key([mod, "control"], "r", lazy.reload_config()),
    Key([mod, "control"], "q", lazy.shutdown()),

    Key([mod], "n", lazy.layout.normalize()),
    Key([mod], "a", lazy.spawn("emacsclient -c -a 'emacs'")),
    Key([mod], "v", lazy.spawn("emacsclient -c -a 'emacs' --eval '(+vterm/here nil)'")),
    Key([mod], "s", lazy.spawn("spectacle -rcb")),
    Key([mod], "t", lazy.spawn("terminator")),
    Key([mod], "f", lazy.spawn("rofi -show drun -theme '~/.config/rofi/config.rasi'")),
    #myAppLauncher = "rofi -show drun -theme '~/.config/rofi/config.rasi'"
    Key([mod], "Tab", lazy.next_layout()),
    Key([mod], "Escape", lazy.window.kill()),
]

groups = [Group(i) for i in "qw"]
for i in groups:
    keys.extend(
        [
            Key(
                [mod],
                i.name,
                lazy.group[i.name].toscreen(),
            ),
        ]
    )

theme = {
    "background": "#282a36",
    "current": "#44475a",
    "selection": "#44475a",
    "foreground": "#f8f8f2",
    "comment": "#6272a4",
    "cyan": "#8be9fd",
    "green": "#50fa7b",
    "orange": "#ffb86c",
    "pink": "#ff79c6",
    "purple": "#bd93f9",
    "red": "#ff5555",
    "yellow": "#f1fa8c",
}

class MyBattery(Battery):
    def build_string(self, status):
        symbols = ""
        index = int(status.percent * 10)
        index = max(index, 0)  # 0 or higher
        index = min(index, 9)  # 9 or lower start at 0
        char = symbols[index]
        if status.state == BatteryState.CHARGING:
            char += ""
            if status.state == BatteryState.UNKNOWN:
                char = ""
        return self.format.format(char=char, percent=status.percent)

battery = MyBattery(
    format="{char} {percent:2.0%}",
    foreground=theme["cyan"],
    notify_below=10,
)

class MyVolume(Volume):
    def create_amixer_command(self, *args):
        return super().create_amixer_command("-M")

    def _update_drawer(self):
        if self.volume <= 0:
            self.volume = "0%"
            self.text = "🔇" + str(self.volume)
        elif self.volume < 30:
            self.text = "🔈" + str(self.volume) + "%"
        elif self.volume < 80:
            self.text = "🔉" + str(self.volume) + "%"
        else:  # self.volume >=80:
            self.text = "🔊" + str(self.volume) + "%"

        def restore(self):
            self.timer_setup()

volume = MyVolume(
    foreground=theme["purple"],
)

def network_con():
    wifi = "wlan0"
    eth = "eth0"
    vpn = "wg0"
    icon = ""
    command = "ip a | egrep 'wlan0|eth0|wg0' | grep 'inet'"
    proc = subprocess.Popen(
        command, universal_newlines=True, shell=True, stdout=subprocess.PIPE
    )
    output = proc.stdout.read()
    words = output.split()
    if vpn in words:
        icon = "旅"
    if eth in words:
        icon = icon + "歷"
    if wifi in words:
        icon = icon + ""
    if icon == "":
        icon = ""
    return icon

def parse_func(text):
    for string in [" - Brave", " - gedit", " - Visual Studio Code"]:
        text = text.replace(string, "")
    return text

layouts = [
    layout.Bsp(
        border_width=5,
        border_focus=theme["purple"],
        border_normal=theme["background"],
        margin=5,
        ),
    layout.Max(),
]

widget_defaults = dict(
    font="mononoki Nerd Font Mono",
    fontsize=20,
    padding=2,
    background=theme["background"],
)
extension_defaults = widget_defaults.copy()

separator = widget.Sep(foreground=theme["foreground"])

screens = [
    Screen(
        wallpaper="~/.config/qtile/wallpaper.jpg",
        wallpaper_mode="stretch",
        top=bar.Bar(
            [
                widget.CurrentLayoutIcon(
                    custom_icon_paths=[(home + "/.config/qtile/icons")],
                    foreground=theme["foreground"],
                    scale=0.8,
                ),
                separator,
                widget.GroupBox(
                    active=theme["green"],
                    background=theme["background"],
                    block_highlight_text_color=theme["purple"],
                    disable_drag=True,
                    foreground=theme["foreground"],
                    highlight_color=theme["selection"],
                    highlight_method="line",
                    inactive=theme["foreground"],
                    urgent_border=theme["red"],
                ),
                widget.WindowName(
                    format="{name}",
                    empty_group_string="",
                    foreground=theme["foreground"],
                    parse_text=parse_func,
                ),

                volume,
                separator,
                battery,
                separator,
                widget.Clock(
                    format=" %b %d %I:%M%p",
                    foreground=theme["yellow"],
                ),
            ],
            22,
        ),


    ),
    Screen(
        wallpaper="~/.config/qtile/wallpaper.jpg",
        wallpaper_mode="stretch",
    ),
]

# Allows dragging floating layouts.
mouse = [
    Drag(
        [mod],
        "Button1",
        lazy.window.set_position_floating(),
        start=lazy.window.get_position(),
    ),
    Drag(
        [mod], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()
    ),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

follow_mouse_focus = True
bring_front_click = True
cursor_warp = False

# Set floating for certain apps
floating_layout = layout.Floating(
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirm"),
        Match(wm_class="dialog"),
        Match(wm_class="download"),
        Match(wm_class="error"),
        Match(wm_class="file_progress"),
        Match(wm_class="notification"),
        Match(wm_class="nm-connection-editor"),
        Match(wm_class="xfce4-power-manager-settings"),
        Match(wm_class="bitwarden"),
        Match(wm_class="blueman-manager"),
        Match(wm_class="Conky"),
        Match(wm_class="kdeconnect-app"),
        Match(wm_class="VirtualBox Machine"),
        Match(wm_class="lxappearance"),
        Match(wm_class="qt5ct"),
        Match(wm_class="xarchiver"),
        Match(wm_class="Clamtk"),
    ]
)

auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# Allows auto-minimize when losing focus for apps that need it
auto_minimize = True


# Functions for changing groups
def window_to_prev_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)


def window_to_next_group(qtile):
    if qtile.currentWindow is not None:
        i = qtile.groups.index(qtile.currentGroup)
        qtile.currentWindow.togroup(qtile.groups[i + 1].name)


@hook.subscribe.startup_once
def autostart():
    subprocess.Popen([home + "/.config/qtile/autostart.sh"])


# java UI toolkits/whitelist
wmname = "LG3D"
