arch:
	sudo pacman -Syu
	sudo pacman -S \
		vim \
		git \
		xorg \
		fish \
		emacs \
		picom \
		conky \
		python3 \
		xmodmap \
		python-pip \
		terminator \
		neofetch \
		curl

	mkdir -p ~/.config/conky/
	ln -s -f `pwd`/conky/DoomOne.conf ~/.config/conky/DoomOne.conf

	mkdir -p ~/.config/doom/
	ln -s -f `pwd`/doom/config.el ~/.config/doom/config.el
	ln -s -f `pwd`/doom/custom.el ~/.config/doom/custom.el
	ln -s -f `pwd`/doom/init.el ~/.config/doom/init.el
	ln -s -f `pwd`/doom/packages.el ~/.config/doom/packages.el

	ln -s -f `pwd`/fish/config.fish ~/.config/fish/config.fish

	mkdir -p ~/.config/neofetch/
	ln -s -f `pwd`/neofetch/config.conf ~/.config/neofetch/config.conf

	mkdir -p ~/.config/qtile/
	ln -s -f `pwd`/qtile/autostart.sh ~/.config/qtile/autostart.sh
	ln -s -f `pwd`/qtile/config.py ~/.config/qtile/config.py
	ln -s -f `pwd`/qtile/wallpaper.jpg ~/.config/qtile/wallpaper.jpg

	mkdir -p ~/.config/rofi/
	ln -s -f `pwd`/rofi/config.rasi ~/.config/rofi/config.rasi
	ln -s -f `pwd`/rofi/dt-center.rasi ~/.config/rofi/dt-center.rasi

push:
	git add *
	git commit -m 'update'
	git push

.PHONY: arch push
