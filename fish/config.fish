if status is-interactive
    # Commands to run in interactive sessions can go here

    export PATH="/home/sebu/.config/emacs/bin:$PATH"

    alias install="sudo pacman -S "
    alias remove="sudo pacman -Rns"
    alias update="sudo pacman -Syu "

    alias out="sudo systemctl restart gdm"
    alias off="shutdown now"
    alias st="git status"
    alias push="git push"
    alias commit="git commit -m "

    alias p="python"

    neofetch

end
